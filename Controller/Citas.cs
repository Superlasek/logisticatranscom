﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogisticaTranscom.Controller
{
    class Citas
    {
        public string[] stringArray = new string[10];
        public Citas()
        {
            stringArray[0] = "No pises las serpientes!";
            stringArray[1] = "No te tomes la vida demasiado en serio. \nNunca saldrás vivo de ella.";
            stringArray[2] = "Un día sin sol es como, tu sabes, la noche.";
            stringArray[3] = "Todo es divertido, siempre y cuando \nle ocurra a otra persona.";
            stringArray[4] = "Previsión meteorológica para esta noche: Oscuro.";
            stringArray[5] = "Es peligroso ahí fuera.  \nAquí, toma esto!";
            stringArray[6] = "Todas las generalizaciones son falsas; incluida ésta.";
            stringArray[7] = "Mantén tu cara a la luz del sol \ny no podrás ver una sombra.";
            stringArray[8] = "No puedes tener una vida positiva \ny una mente negativa.";
            stringArray[9] = "La calidad es responsabilidad de todos.";
        }
    }
}

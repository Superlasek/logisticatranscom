﻿/*
 * Transcom Software
 * ---- AUTHOR  ------
 * Alexis Carlos Stephano Bravo Castañeda
 * Erika Rubi Robles Ramos
 * Alan Fletes Ascanio
 * Carlos Adrian Dominguez Barajas
 * 
 * UNEDL 4°A T/M
 * Ing. en Software
*/

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LogisticaTranscom
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            BtnMaximizar.Visible = false;
            BtnRestaurar.Visible = true;
        }

        private void BtnRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            BtnRestaurar.Visible = false;
            BtnMaximizar.Visible = true;
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        [DllImport("User32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("User32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void AbrirFormHija(object formHija)
        {
            if (this.panelContenedor.Controls.Count > 0)
                this.panelContenedor.Controls.RemoveAt(0);
            Form fh = formHija as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.panelContenedor.Controls.Add(fh);
            this.panelContenedor.Tag = fh;
            fh.Show();

        }

        private void Btnproductos_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new productos());
        }

        private void btnclientes_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new clientes());
        }

        private void BtnInicio_Click(object sender, EventArgs e)
        {
            AbrirFormHija(new inicio());
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BtnInicio_Click(null, e);
        }

        private void horafecha_Tick(object sender, EventArgs e)
        {
            labelHora.Text = DateTime.Now.ToString("HH:mm:ss");
            //labelHora.Text = DateTime.Now.ToLongTimeString();
            //labelFecha.Text = DateTime.Now.ToShortDateString();
            labelFecha.Text = DateTime.Now.ToString("dd/MMMM/yyy");
        }
    }
}

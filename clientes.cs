﻿/*
 * Transcom Software
 * ---- AUTHOR  ------
 * Alexis Carlos Stephano Bravo Castañeda
 * Erika Rubi Robles Ramos
 * Alan Fletes Ascanio
 * Carlos Adrian Dominguez Barajas
 * 
 * UNEDL 4°A T/M
 * Ing. en Software
*/

// Icono padlock por: Kiranshastry de flaticon
// Icono user por: Becris de flaticon
// Icono zoom por: phatplus de flaticon

using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;


namespace LogisticaTranscom
{
    public partial class clientes : Form
    {
        // STRINGS A LA BASE DE DATOS

        //string connectionString = @"Server=localhost;Database=clientesdb;Uid=root;Pwd=E1r9MySQL#0;";
        //string connectionString = @"Server=localhost;Database=clientesdb;Uid=root;Pwd=sqlLSKsql8;";
        string connectionString = @"Server=localhost;Database=transcomdb;Uid=root;Pwd=sqlLSKsql8;";
        int idClientes = 0;

        public clientes()
        {
            InitializeComponent();
        }

        // Boton Guardar
        private void buttonGuardarCliente_Click(object sender, EventArgs e)
        {
            // Validaciones
            if (txtNombre.Text == "" || txtApellido.Text == "" || txtTelefono.Text == "" 
                || txtEmail.Text == "" || txtEmpresa.Text == "")
                MessageBox.Show("Favor de llenar los campos vacios.");
            else

                // Conexion a la DB y guarda el producto
                using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
                {
                    mysqlCon.Open();
                    MySqlCommand mySqlCmd = new MySqlCommand("clienteAddorQuit", mysqlCon);
                    mySqlCmd.CommandType = CommandType.StoredProcedure;
                    mySqlCmd.Parameters.AddWithValue("_idClientes", idClientes);
                    mySqlCmd.Parameters.AddWithValue("_Nombre", txtNombre.Text.Trim());
                    mySqlCmd.Parameters.AddWithValue("_Apellidos", txtApellido.Text.Trim());
                    mySqlCmd.Parameters.AddWithValue("_Telefono", txtTelefono.Text.Trim());
                    mySqlCmd.Parameters.AddWithValue("_Email", txtEmail.Text.Trim());
                    mySqlCmd.Parameters.AddWithValue("_Empresa", txtEmpresa.Text.Trim());
                    mySqlCmd.ExecuteNonQuery();
                    MessageBox.Show("Se añadio correctamente.");
                    Limpiar();
                    GridFill();
                }
        }

        // Llenado de la tabla
        void GridFill()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                try
                {
                    mysqlCon.Open();
                    MySqlDataAdapter sqlDa = new MySqlDataAdapter("ClientesListar", mysqlCon);
                    sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                    DataTable dtblclientes = new DataTable();
                    sqlDa.Fill(dtblclientes);
                    dataGridViewCliente.DataSource = dtblclientes;
                    //dataGridViewCliente.Columns[0].Visible = false;
                }
                catch
                {
                    MessageBox.Show("No se ha conectado a la base de datos.");
                }
            }
        }

        // Carga la tabla al cargar el container con datos limpios
        private void clientes_Load(object sender, EventArgs e)
        {
            Limpiar();
            GridFill();
        }

        // Limpia todo campo
        void Limpiar()
        {
            txtNombre.Text = txtApellido.Text = txtTelefono.Text = txtEmail.Text = txtEmpresa.Text = "";
            idClientes = 0;
            buttonGuardarCliente.Text = "Guardar";
            buttonBorrarCliente.Enabled = false;
        }

        // Doble click a la tabla en un dato establece el valor en los campos
        // Nos sirve para actualizar al mismo tiempo
        private void dataGridViewCliente_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridViewCliente.CurrentRow.Index != -1)
            {
                txtNombre.Text = dataGridViewCliente.CurrentRow.Cells[1].Value.ToString();
                txtApellido.Text = dataGridViewCliente.CurrentRow.Cells[2].Value.ToString();
                txtTelefono.Text = dataGridViewCliente.CurrentRow.Cells[3].Value.ToString();
                txtEmail.Text = dataGridViewCliente.CurrentRow.Cells[4].Value.ToString();
                txtEmpresa.Text = dataGridViewCliente.CurrentRow.Cells[5].Value.ToString();

                idClientes = Convert.ToInt32(dataGridViewCliente.CurrentRow.Cells[0].Value.ToString());
                buttonGuardarCliente.Text = "Actualizar";
                buttonBorrarCliente.Enabled = Enabled;
            }
        }

        // Boton Buscar
        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlDataAdapter sqlDa = new MySqlDataAdapter("clientesSearchByBuscarValor", mysqlCon);
                sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDa.SelectCommand.Parameters.AddWithValue("_BuscarValor", txtBuscar.Text);
                DataTable dtblclientes = new DataTable();
                sqlDa.Fill(dtblclientes);
                dataGridViewCliente.DataSource = dtblclientes;
                dataGridViewCliente.Columns[0].Visible = false;
            }
        }

        // Label X para limpiar lo que tecleo el usuario en Buscar
        private void label6_Click(object sender, EventArgs e)
        {
            txtBuscar.Text = "";
            GridFill();
        }


        // Boton Cancelar
        private void buttonCancelarCliente_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        // Boton Borrar
        private void buttonBorrarCliente_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlCommand mySqlCmd = new MySqlCommand("clienteDeletebyID", mysqlCon);
                mySqlCmd.CommandType = CommandType.StoredProcedure;
                mySqlCmd.Parameters.AddWithValue("_idClientes", idClientes);
                mySqlCmd.ExecuteNonQuery();
                MessageBox.Show("Se borro correctamente.", "Se borro el registro.");
                Limpiar();
                GridFill();
            }
        }


        // Keypress Nombre
        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo Letras", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        // Keypress Apellido
        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                MessageBox.Show("Solo Letras", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }

        }

        // Keypress Telefono
        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtTelefono.Text.Length >= 13)
            {
                e.Handled = true;
                return;
            }
            if ((e.KeyChar >= 32 && e.KeyChar <= 47) || (e.KeyChar >= 58 && e.KeyChar <= 255))
            {
                MessageBox.Show("Solo Numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;

            }

        }

        // Keypress Email
        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

    }
}

﻿/*
 * Transcom Software
 * ---- AUTHOR  ------
 * Alexis Carlos Stephano Bravo Castañeda
 * Erika Rubi Robles Ramos
 * Alan Fletes Ascanio
 * Carlos Adrian Dominguez Barajas
 * 
 * UNEDL 4°A T/M
 * Ing. en Software
*/

using LogisticaTranscom.Controller;
using System;
using System.Windows.Forms;

namespace LogisticaTranscom
{
    public partial class inicio : Form
    {
        public inicio()
        {
            InitializeComponent();
            EstablecerCita();
        }


        private void EstablecerCita()
        {
            Citas citas = new Citas();
            Random rand = new Random();
            labelCita.Text = citas.stringArray[rand.Next(0, 9)]; // Usa un pseudo-random desde Citas.cs
        }

    }
}

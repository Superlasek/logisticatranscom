﻿/*
 * Transcom Software
 * ---- AUTHOR  ------
 * Alexis Carlos Stephano Bravo Castañeda
 * Erika Rubi Robles Ramos
 * Alan Fletes Ascanio
 * Carlos Adrian Dominguez Barajas
 * 
 * UNEDL 4°A T/M
 * Ing. en Software
*/

using System;
using System.Windows.Forms;

namespace LogisticaTranscom
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBoxUsuarioNombre.Text == "admin" && textBoxContrasenia.Text == "adminpass")
            {
                new Form1().Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("El nombre de usuario o contraseña no es correcto, intenta de vuelta.", "Error al iniciar sesión.");
                textBoxContrasenia.Clear();
                textBoxUsuarioNombre.Clear();
                textBoxUsuarioNombre.Focus();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            textBoxUsuarioNombre.Clear();
            textBoxContrasenia.Clear();
            textBoxUsuarioNombre.Focus();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labelMagia_Click(object sender, EventArgs e)
        {
            new Form1().Show();
            this.Hide();
        }
    }
}

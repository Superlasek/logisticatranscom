﻿/*
 * Transcom Software
 * ---- AUTHOR  ------
 * Alexis Carlos Stephano Bravo Castañeda
 * Erika Rubi Robles Ramos
 * Alan Fletes Ascanio
 * Carlos Adrian Dominguez Barajas
 * 
 * UNEDL 4°A T/M
 * Ing. en Software
*/

// Icono padlock por: Kiranshastry de flaticon
// Icono user por: Becris de flaticon
// Icono zoom por: phatplus de flaticon

using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;


namespace LogisticaTranscom
{
    public partial class productos : Form
    {
        // STRINGS A LA BASE DE DATOS

        //string connectionString = @"Server=localhost;Database=productosdb;Uid=root;Pwd=E1r9MySQL#0;";
        //string connectionString = @"Server=localhost;Database=productosdb;Uid=root;Pwd=sqlLSKsql8;";
        string connectionString = @"Server=localhost;Database=transcomdb;Uid=root;Pwd=sqlLSKsql8;";
        int productoID = 0;

        public productos()
        {
            InitializeComponent();
        }

        // Boton Guardar
        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            // Validaciones
            if (textBoxProductoNombre.Text == "" || textBoxProductoProveedor.Text == ""
                || textBoxProductoFechaEntrada.Text == "" || spinnerCantidad.Value == 0)
                MessageBox.Show("Favor de llenar los campos vacios.");
            else

                // Conexion a la DB y guarda el producto
                using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
                {
                    mysqlCon.Open();
                    MySqlCommand mySqlCmd = new MySqlCommand("ProductoAnadirQuitar", mysqlCon);
                    mySqlCmd.CommandType = CommandType.StoredProcedure;
                    mySqlCmd.Parameters.AddWithValue("_ProductoID", productoID);
                    mySqlCmd.Parameters.AddWithValue("_ProductoNombre", textBoxProductoNombre.Text.Trim());
                    mySqlCmd.Parameters.AddWithValue("_ProductoCantidad", spinnerCantidad.Value);
                    mySqlCmd.Parameters.AddWithValue("_ProductoProveedor", textBoxProductoProveedor.Text.Trim());
                    mySqlCmd.Parameters.AddWithValue("_ProductoFechaEntrada", textBoxProductoFechaEntrada.Text.Trim());
                    mySqlCmd.ExecuteNonQuery();
                    MessageBox.Show("Se añadio correctamente.", "Se guardo el registro.");
                    Limpiar();
                    GridFill();
                }
        }

        // Llenado de la tabla
        void GridFill()
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                try
                {
                    mysqlCon.Open();
                    MySqlDataAdapter sqlDa = new MySqlDataAdapter("ProductoListar", mysqlCon);
                    sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                    DataTable dtblProducto = new DataTable();
                    sqlDa.Fill(dtblProducto);
                    dataGridViewProducto.DataSource = dtblProducto;
                    //dataGridViewProducto.Columns[0].Visible = false;
                }
                catch
                {
                    MessageBox.Show("No se ha conectado a la base de datos.");
                }
            }
        }

        // Carga la tabla al cargar el container con datos limpios
        private void productos_Load(object sender, EventArgs e)
        {
            Limpiar();
            GridFill();
        }

        // Limpia todo campo
        void Limpiar()
        {
            textBoxProductoNombre.Text = textBoxProductoProveedor.Text = textBoxProductoFechaEntrada.Text 
                = textBoxBuscarProducto.Text = "";
            spinnerCantidad.Value = 0;
            productoID = 0;
            buttonGuardar.Text = "Guardar";
            buttonBorrar.Enabled = false;
        }

        // Doble click a la tabla en un dato establece el valor en los campos
        // Nos sirve para actualizar al mismo tiempo
        private void dataGridViewProducto_DoubleClick(object sender, EventArgs e)
        {
            if (dataGridViewProducto.CurrentRow.Index != -1)
            {
                textBoxProductoNombre.Text = dataGridViewProducto.CurrentRow.Cells[1].Value.ToString();
                //>>>> Estos no sirven pero los dejo por referencia
                //spinnerCantidad = Convert.ToInt32(dataGridViewProducto.CurrentRow.Cells[2].Value);
                //int spinnerCantidad = Convert.ToInt32(dataGridViewProducto.CurrentRow.Cells[2].Value);
                //spinnerCantidad.Value = (decimal)dataGridViewProducto.CurrentRow.Cells[2].Value;
                spinnerCantidad.Value = (int)dataGridViewProducto.CurrentRow.Cells[2].Value;
                textBoxProductoProveedor.Text = dataGridViewProducto.CurrentRow.Cells[3].Value.ToString();
                textBoxProductoFechaEntrada.Text = dataGridViewProducto.CurrentRow.Cells[4].Value.ToString();
                
                productoID = Convert.ToInt32(dataGridViewProducto.CurrentRow.Cells[0].Value.ToString());
                buttonGuardar.Text = "Actualizar";
                buttonBorrar.Enabled = Enabled;
            }
        }

        // Boton Buscar
        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlDataAdapter sqlDa = new MySqlDataAdapter("ProductoBuscarPorValor", mysqlCon);
                sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDa.SelectCommand.Parameters.AddWithValue("_BuscarValor", textBoxBuscarProducto.Text);
                DataTable dtblProducto = new DataTable();
                sqlDa.Fill(dtblProducto);
                dataGridViewProducto.DataSource = dtblProducto;
                dataGridViewProducto.Columns[0].Visible = false;
            }
        }

        // Label X para limpiar lo que tecleo el usuario en Buscar
        private void label1_Click(object sender, EventArgs e)
        {
            textBoxBuscarProducto.Text = "";
            GridFill();
        }

        // Boton Cancelar
        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        // Boton Borrar
        private void buttonBorrar_Click(object sender, EventArgs e)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlCommand mySqlCmd = new MySqlCommand("ProductoBorrarPorID", mysqlCon);
                mySqlCmd.CommandType = CommandType.StoredProcedure;
                mySqlCmd.Parameters.AddWithValue("_ProductoID", productoID);
                mySqlCmd.ExecuteNonQuery();
                MessageBox.Show("Se borro correctamente.", "Se borro el registro.");
                Limpiar();
                GridFill();
            }
        }

        // Keypress FechaEntrada
        private void textBoxProductoFechaEntrada_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limite de 10 digitos
            if (textBoxProductoFechaEntrada.Text.Length >= 10)
            {
                MessageBox.Show("Solo Formato dd/MM/YYYY ", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            else
            {
                // No se permiten letras, unicamente numeros
                if ((e.KeyChar >= 32 && e.KeyChar <= 46) || (e.KeyChar >= 58 && e.KeyChar <= 255))
                {
                    MessageBox.Show("Solo Numeros", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Handled = true;
                    return;

                }
            }
        }
    }
}
